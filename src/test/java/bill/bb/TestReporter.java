package bill.bb;

import org.junit.Test;

import bill.bb.util.Reporter;

public class TestReporter {
	
	@Test
	public void testRun() {
		
		Reporter myReporter = new Reporter(new int[] {1,2,3});
		
		myReporter.addReport(new int[] {1, 9 ,8});
		
		myReporter.addReport(new int[] {4, 5 ,6});
		
		
		myReporter.addReport(new int[] {3, 1 ,2});
		
		myReporter.addReport(new int[] {1, 2 ,3});
		
	}

}
