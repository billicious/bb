package bill.bb.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Reporter {
	
	public Reporter (int[] aNum) {
		
		if (aNum == null) {
			List<Integer> myRanNum = new ArrayList<Integer>();
			int myNextRan = Reporter.randInt(0, 9);
			myRanNum.add(myNextRan);
			for (int i = 0 ; i < 2 ; i++) {
				myNextRan = Reporter.randInt(0, 9);
				while (myRanNum.contains(myNextRan)) {
					myNextRan = Reporter.randInt(0, 9);
				}
				myRanNum.add(myNextRan);
			}
			theNum = new Integer[] {myRanNum.get(0), myRanNum.get(1), myRanNum.get(2)}; 
		} else {
			System.out.println("The hidden number: " + Arrays.toString(aNum));
			for (int i = 0 ; i < 3 ; i++)  {
				theNum[i] = aNum[i];
			}
		}
		
		theReport = new LinkedHashMap<String, String>();
	}
	public Reporter() {
		
		this(null);
	}
	
	public void run() {
		boolean myIsAns = false;

    	try (BufferedReader myBR = new BufferedReader(new InputStreamReader(System.in))) {

    		while (myIsAns == false) {
    		
	    		System.out.print(">> ");
	    		String myInputStr = myBR.readLine();
	
	    		String[] myInputArray = myInputStr.split(",");
	
	    		if (myInputArray.length == 1) {
	    			myIsAns = this.addReport(myInputArray[0]);
	    		} else {
	    			int [] myInputInt = new int [3];
	    			
	    			for (int i = 0 ; i < 3 ; i++){
	    				myInputInt[i] = Integer.parseInt(myInputArray[i].replaceAll("\\s", ""));
	    			}
	    			myIsAns = this.addReport(myInputInt);
	    		}
    		}
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
	}
	
	public boolean addReport(String aDontKnow) {
		if (aDontKnow.equals("?")) {
			System.out.println("The hidden number: " + Arrays.toString(theNum));
			return true;
		} else {
			return false;
		}
			
	}

	public boolean addReport(int[] aNum) {
		
		String myNumInStr = Arrays.toString(aNum);
		
		if (theReport.containsKey(myNumInStr)) {
			System.out.println(myNumInStr + " is already exist");
			return false;
		} else {
			
			Map<String, Integer> myRes = checkNum(aNum);
			
			StringBuilder myResInStr = new StringBuilder(); 
			for (Map.Entry<String, Integer> myEntry : myRes.entrySet()) {
				myResInStr.append(myEntry.getKey() + "[" + myEntry.getValue() + "] ");
			}
			System.out.println(myNumInStr + "\t" + myResInStr.toString());
			theReport.put(myNumInStr, myResInStr.toString());
			
			if (myRes.get("S") == 3) {
				System.out.println("You won !!");
				return true;
			}
			return false;
		}
	}
	
	
	public Map<String, Integer> checkNum(int[] aNum) {
		
		Map<String, Integer> myRes = new TreeMap<String, Integer>();
		myRes.put("B", 0);
		myRes.put("S", 0);
		List<Integer> myNumAsList = Arrays.asList(theNum); 
		for (int i = 0 ; i < 3 ; i++) {
			if (myNumAsList.contains(aNum[i])) {
				if (theNum[i] == aNum[i]) {
					myRes.put("S", myRes.get("S") + 1);				
					} else {
					myRes.put("B", myRes.get("B") + 1);
				}
			}
		}
		
		if (myRes.get("B") == 0 && myRes.get("S") == 0) {
			myRes.put("O", 1);
		}
		 
		return myRes;
	}
	
	private static int randInt(int aMin, int aMax) {
		Random myRan = new Random();
		
		return myRan.nextInt((aMax - aMin) + 1) + aMin;
	}
	
	
	private Map<String, String> theReport = null; 
	private Integer[] theNum = new Integer[3];
}
